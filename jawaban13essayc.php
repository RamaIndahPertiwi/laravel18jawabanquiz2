Membuat table
	
	CREATE TABLE customers (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name varchar (255),
	email varchar (255),
	password varchar (255));
	
	CREATE TABLE orders(
	id INT AUTO_INCREMENT PRIMARY KEY,
	amount varchar (255),
	customer_id int);
	
	alter table orders
    add foreign key (customer_id) references customers (id);